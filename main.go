package main

import (
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
)

func main() {
	message := "Hello!"
	port := "8088"
	if len(os.Args) > 1 { // Optionally pass in a git repo as a command line argument
        message = os.Args[1]
		port = os.Args[2]
	}
	r := gin.Default()
	r.GET("/", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"greeting": greeting(),
		})
	})
	r.Run("localhost:", port) // listen and serve on 0.0.0.0:8088 (for windows "localhost:8088")
}

func greeting(msg string) string {
	return msg
}
